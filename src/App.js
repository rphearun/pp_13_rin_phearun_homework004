import logo from './logo.svg';
import './App.css';
import Input from './components/Input';

function App() {
  return (
    <div className="App bg-green-300 w-full h-screen  ">
      <Input/>     
    </div>
  );
}

export default App;
