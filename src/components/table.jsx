import React, { Component } from "react";
import Swal from "sweetalert2";

export default class Table extends Component {
  constructor() {
    super();
    this.state = {
      status: false,
    };
  }
  onClick = (e) => {
    this.setState({ status: !this.state.status });
  };

  popUp = (item) => {
    Swal.fire({
      html:
        "<b> ID: " +
        item.id +
        "<br>Email: " +
        item.email +
        "<br> Username: " +
        item.Pname +
        "<br> Age: " +
        item.age +
        "</b> ",
      width: 600,
      padding: "3em",
      color: "#716add",
      background: "#fff url(/images/trees.png)",
      backdrop: `
              rgba(0,0,123,0.4)
              url("/images/nyan-cat.gif")
              left top
              no-repeat
            `,
    });
  };

  render() {
    return (
      <div>
        <div class="w-1/2 m-auto">
          <table class="w-full text-sm text-left text-gray-500  ">
            <thead class="text-xs text-gray-700 uppercase text-center">
              <tr>
                <th scope="col" class="px-6 py-3">
                  ID
                </th>
                <th scope="col" class="px-6 py-3">
                  Email
                </th>
                <th scope="col" class="px-6 py-3">
                  username
                </th>
                <th scope="col" class="px-6 py-3">
                  age
                </th>
                <th scope="col" class="px-6 py-3">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item) => (
                <tr key={item.id} class="border-b bg-gray-50 even:bg-pink-200 ">
                  <td class="px-6 py-4">{item.id}</td>
                  <td class="px-6 py-4">{item.email}</td>
                  <td class="px-6 py-4">{item.Pname}</td>
                  <td class="px-6 py-4">{item.age}</td>
                  <td class="px-6 py-4 flex gap-4">
                    <button
                      type="button"
                      style={
                        item.status == "pending"
                          ? { background: "red" }
                          : { background: "green" }
                      }
                      class=" px-5 py-2.5 mr-2 mb-2 text-white  hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm  text-center "
                      onClick={() => this.props.data1(item.id)}
                    >
                      {item.status}
                    </button>
                    <button
                      type="button"
                      class="px-5 py-2.5 mr-2 mb-2 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm  w-full"
                      onClick={() => this.popUp(item)}
                    >
                      Show more
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
