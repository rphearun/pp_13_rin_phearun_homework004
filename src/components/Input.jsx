import React, { Component } from "react";
import Table from "./table";

export default class Input extends Component {
  constructor() {
    super();
    this.state = {
      person: [
        // {id:, email: "rphearun22@gmail.com" , Pname : "Phearun" , age : "21" , status : "padding"},
      ],
      newMail: "",
      newPname: "",
      newAge: "",
    };
  }

  emailChange = (e) => {
    this.setState({
      newMail: e.target.value,
    });
  };
  nameChange = (e) => {
    this.setState({
      newPname: e.target.value,
    });
  };
  ageChange = (e) => {
    this.setState({
      newAge: e.target.value,
    });
  };

  handleSubmit = () => {
    const newObj = {
      id: this.state.person.length + 1,
      email: this.state.newMail ==="" ? "null" : this.state.newMail,
      Pname: this.state.newPname === "" ? "null" : this.state.newPname,
      age: this.state.newAge === "" ? "null" : this.state.newAge,
      status: "pending",
    };

    this.setState({
      person: [...this.state.person, newObj],
      newMail: "",
      newPname: "",
      newAge: "",
    });
  };
  changeStatus = (e) => {
    console.log(e);

    this.setState({
      person: this.state.person.map((item) => {
        let change = item;
        if (item.id === e) {
          change.status = item.status === "done" ? "pending" : "done";
          return change;
        }
        return change;
      }),
    });
  };

  render() {
    return (
      <div>
        <form>
          <div class="w-full bg-green-300 h-full">
            <h1 className="font-bold text-transparent text-center text-6xl bg-clip-text bg-gradient-to-r from-red-500 via-cyan-400 to-pink-600">
              Please fill your information
            </h1>
            <div className="m-auto w-2/3 ">
              <div className="mb-8">
                <label
                  for="email"
                  class="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white "
                >
                  Email address
                </label>
              <div className="flex">
                <span className="inline-flex items-center px-3  text-md bg-cyan-300 border border-r-0 border-gray-300 rounded-l-md">
                📧
                </span>
              <input
                  type="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="konkhmer@gmail.com"
                  onChange={this.emailChange}
                  required
                />
              </div>
               
              </div>
              <div className="mb-8">
                <label
                  for="name"
                  class=" text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Username
                </label>
                <div className="flex">
                <span className="inline-flex items-center px-3  text-md bg-yellow-400 border border-r-0 border-gray-300 rounded-l-md">
                👨🏽‍💻
                </span>
                <input
                  type="text"
                  id="name"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Doe"
                  onChange={this.nameChange}
                  required
                />

                </div>
               
              </div>
              <div>
                <div className="mb-8">
                  <label
                    for="age"
                    class=" text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Age
                  </label>
                  <div className="flex">
                  <span className="inline-flex items-center px-3  text-sm bg-pink-500 border border-r-0 border-gray-300 rounded-l-md">
                  📅 
                </span>
                  <input
                    type="text"
                    id="age"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="21"
                    onChange={this.ageChange}
                    required
                  />
                  </div>
                 
                </div>
              </div>
            </div>
            <button
              type="button"
              class="mt-2 mb-5 px-20 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              onClick={this.handleSubmit}
            >
              Register
            </button>
            <hr />
            <br />
            <Table data={this.state.person} data1={this.changeStatus} />
          </div>
        </form>
      </div>
    );
  }
}
